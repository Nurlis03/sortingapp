package org.example;

import java.util.Arrays;

public class App
{
    public static void main( String[] args )
    {
        if (args.length == 0) {
            System.out.println("No arguments provided.");
            return;
        }

        if (args.length > 10) {
            System.out.println("Error: More than 10 arguments provided.");
            return;
        }

        try {
            int[] numbers = new int[args.length];
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
            Arrays.sort(numbers);
            for (int number : numbers) {
                System.out.println(number);
            }
        } catch (NumberFormatException e) {
            System.out.println("Error: All arguments must be integers.");
        }
    }
}
