import org.example.App;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    // Parameters for parameterized tests
    @Parameters(name = "{index}: {0} => \"{1}\"")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "No arguments", new String[]{}, "No arguments provided." + System.lineSeparator() },
                { "One argument", new String[]{"5"}, "5" + System.lineSeparator() },
                { "Ten arguments", new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"},
                        "1" + System.lineSeparator() +
                                "2" + System.lineSeparator() +
                                "3" + System.lineSeparator() +
                                "4" + System.lineSeparator() +
                                "5" + System.lineSeparator() +
                                "6" + System.lineSeparator() +
                                "7" + System.lineSeparator() +
                                "8" + System.lineSeparator() +
                                "9" + System.lineSeparator() +
                                "10" + System.lineSeparator() },
                { "More than ten arguments", new String[]{"11", "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"},
                        "Error: More than 10 arguments provided." + System.lineSeparator() },
                { "Non-integer argument", new String[]{"10", "a", "8"},
                        "Error: All arguments must be integers." + System.lineSeparator() }
        });
    }

    @Test
    public void testSortingWithArguments() {
        App.main(arguments);
        assertEquals(expectedOutput, outContent.toString());
    }

    private final String[] arguments;
    private final String expectedOutput;

    public SortingTest(String testName, String[] arguments, String expectedOutput) {
        this.arguments = arguments;
        this.expectedOutput = expectedOutput;
    }
}
